var UserDB = {};
(function (udb) {
    var users = Storage.load('users');

    udb.getAll = function(){
      return users;
    }

    udb.saveMessage = function(name, message){
      var user = users[name];

      user.messages += message+".";
      console.log(name+"s messages - "+user.messages);

      Storage.save('users', users);
    }

    udb.getMessages = function(name){
      var user = users[name];
      return user.messages.split(".");

    }

    udb.hasOptedIn = function(name){
      return users[name].opt_in;
    }

    udb.load = function (name) {
        return users[name];
    };

    udb.has = function (name) {
        return !!users[name];
    };

    udb.sessionExists = function () {
        return !!users.current;
    };

    function splitKeys(key) {
        return key.split('.');
    }

    function walkPath(path, obj) {
        if (path.length == 1) {
            return obj[path[0]];
        }
        return walkPath(path.slice(1), obj[path[0]]);
    }

    udb.get = function (key) {
        var path = splitKeys(key);
        return walkPath(path, users.current);
    };

    udb.__get = function () {
        return users.current;
    };

    udb.save = function (name) {
        var user = {name: name};
        user.messages = "";
        user.opt_in = true;
        users[name] = user;
        Storage.save('users', users);
    };

    udb.setSession = function (name) {
        users.current = users[name];
    };

    udb.saveSession = function () {
        users[users.current.name] = users.current;
        Storage.save('users', users);
    };

    udb.reset = function () {
        Storage.clear();
    }
}(UserDB));


var User = {};
(function (us) {
    us.login = function (name) {
        if (UserDB.has(name)) {
            goHome(name);
        } else {
            alert('Please create an account first');
        }
    };

    function goHome(name) {
        UserDB.setSession(name);
        UserDB.saveSession();
        Router.go('dashboard.html');
    }

    us.register = function (name) {
        UserDB.save(name);
        goHome(name);
    };

    function update() {
        UserDB.saveSession();
        Router.go('dashboard.html');
    }

    function updateWithoutRefresh(){
        UserDB.saveSession();
    }

    us.recharge = function (price) {
        Migration.recharge(UserDB.__get(), price);
        updateWithoutRefresh();
    };

    us.changePlan = function (tariff) {
        UserDB.__get().tariff = tariff;
        update();
    };

    us.opt_in = function(){
      var user = UserDB.__get();
      user.opt_in = true;
      update();

    }

    us.opt_out = function(){
      var user = UserDB.__get();
      user.opt_in = false;
      update();
    }

}(User));
