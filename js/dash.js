function init(){
  $('#user').text(UserDB.get('name').split()[0]);
  //$('.tab_item a[href="#'+UserDB.__get()+'"]').tab('show');
  var tab_class = "";
  users =  UserDB.getAll();
  for (user in users){
    if(user != "current"){
    document.getElementById("users_tab").innerHTML +=
    '<li><a class="tab_item" data-toggle="tab" href="#'+user+'" class="w3-large" \
    style="text-decoration: none;">'+user+'</a></li>';

    var stat = {opt_type:"opt-in", color:"green"};
    if (UserDB.hasOptedIn(user) == true){
      stat.opt_type = "opt-out";
      stat.color = "red";
    }
    if (user == UserDB.__get().name){
      tab_class = "opt tab-pane fade in active";
    }else{
      tab_class = "opt tab-pane fade in";
    }
    var things = UserDB.getMessages(user);
    var mes = "";
    for (var i = 0; i < things.length; i++){
      mes += "<br/><span>"+things[i]+"</span>";
    }
    document.getElementById("tab_content").innerHTML += '\
      <div id="'+user+'" class="'+tab_class+'"> \
          <button type="submit" class="w3-btn w3-round-small w3-xlarge w3-'+stat.color+'"\
           style="float:right">'+stat.opt_type+'</button> \
           <div style="margin: 0 auto; width:100%">'+mes+'</div>\
      </div>';
}
  }


  $(".opt").click(function(evt){
    var user = UserDB.__get().name;

    if (($("#"+user+" > button")).html() == "opt-in"){
      User.opt_in();
    }else{
      User.opt_out();
    }
  });

  $('#btnMessage').click(function(evt) {
      evt.preventDefault();
      var message = $('#txtMessage').val();
      pushMessage(message);
  });

  $('.tab_item').click(function(evt) {
      evt.preventDefault();
      var currentUser = $(this).html();
      User.login(currentUser);
  });
}

function pushMessage(message){
  users = UserDB.getAll();
  for (user in users){
    if (UserDB.hasOptedIn(user) == true){
      UserDB.saveMessage(user, message);
    }
  }
}

function updateMessages(){
}

function message() {
    $('#btnSend').click(function (evt) {
        // evt.preventDefault();
        console.log('Calling..addsds')
        User.message();
    })
}

function dash() {
    init();
}

$(document).ready(dash);
